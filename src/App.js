//App.js
import React, {Component} from 'react'
import { Increment, Decrement } from './redux'
import {connect} from 'react-redux'

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>{this.props.count}</h1> 
        <button onClick={this.props.Increment}>+</button>
        <button onClick={this.props.Decrement}>-</button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    count: state.count
})

const mapDispatchToProps = {
    Increment,
    Decrement
}

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App)

export default AppContainer